import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {HorizontalBar} from 'react-chartjs-2';
import { Button } from 'reactstrap';

class Graph extends Component{
  state = {
    showVolume: false
  }
  changeToggleVolume(){
    this.setState({showVolume: !this.state.showVolume})
  }
  render () {
    const {data} = this.props, {showVolume} = this.state;
    let labels = [];
    let priceData = {
      openData: [],
      highData: [],
      lowData: [],
      closeData: [],
      adjustedData: [],
      volumeData: [],
      amountData: [],
      coefficientData: [],
    }
    for(let key in data["Time Series (Daily)"]){
      labels.push(key);
      priceData.openData.push(+data["Time Series (Daily)"][key]["1. open"]);
      priceData.highData.push(+data["Time Series (Daily)"][key]["2. high"]);
      priceData.lowData.push(+data["Time Series (Daily)"][key]["3. low"]);
      priceData.closeData.push(+data["Time Series (Daily)"][key]["4. close"]);
      priceData.adjustedData.push(+data["Time Series (Daily)"][key]["5. adjusted close"]);
      priceData.volumeData.push(+data["Time Series (Daily)"][key]["6. volume"]);
      priceData.amountData.push(+data["Time Series (Daily)"][key]["7. dividend amount"]);
      priceData.coefficientData.push(+data["Time Series (Daily)"][key]["8. split coefficient"]);
    }

    const mostData = {
      labels: labels,
      datasets: [
        {
          label: 'Open',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,99,132,0.8)',
          hoverBorderColor: 'rgba(255,99,132,1)',
          data: priceData.openData
        },
        {
          label: 'High',
          backgroundColor: 'rgba(255,170,132,0.2)',
          borderColor: 'rgba(255,170,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,170,132,0.8)',
          hoverBorderColor: 'rgba(255,170,132,1)',
          data: priceData.highData
        },
        {
          label: 'Low',
          backgroundColor: 'rgba(230,200,99,0.2)',
          borderColor: 'rgba(230,200,99,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(230,200,99,0.4)',
          hoverBorderColor: 'rgba(230,200,99,1)',
          data: priceData.lowData
        },
        {
          label: 'Close',
          backgroundColor: 'rgba(99,200,132,0.2)',
          borderColor: 'rgba(99,200,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(99,200,132,0.4)',
          hoverBorderColor: 'rgba(99,200,132,1)',
          data: priceData.closeData
        },
        {
          label: 'Adjusted Close',
          backgroundColor: 'rgba(99,99,200,0.2)',
          borderColor: 'rgba(99,99,200,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(99,99,200,0.4)',
          hoverBorderColor: 'rgba(99,99,200,1)',
          data: priceData.adjustedData
        }
      ]
    };

    const volumeData = { labels: labels,
      datasets: [
        {
        label: 'Volume',
        backgroundColor: 'rgba(230,99,230,0.2)',
        borderColor: 'rgba(230,99,230,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(230,99,230,0.4)',
        hoverBorderColor: 'rgba(230,99,230,1)',
        data: priceData.volumeData
      }]
    };

      if(showVolume){
        return (  <div>
          <h4 className="message">Dividend amount is 0.0000 and split coefficient is 1.0000</h4>
          <button onClick={() => this.changeToggleVolume()}>Toggle Volume Graph: {""+showVolume}</button>
          <HorizontalBar data={volumeData} height={1000}/>
        </div>);
      }else{
        return (
          <div>
            <h4 className="message">Dividend amount is 0.0000 and split coefficient is 1.0000</h4>
            <Button color="info" onClick={() => this.changeToggleVolume()}>Toggle Volume Graph: {""+showVolume}</Button>
            <HorizontalBar data={mostData} height={1000}/>
          </div>
        );
      }
  }
}

export default Graph
