import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import "react-table/react-table.css";

class Table extends Component{
  render () {
    console.log(this.props);
    const {data} = this.props;
    let dataTable = [];

    for(let key1 in data["Time Series (Daily)"]){
      let obj = {};
      obj.date = key1;
      for(let key2 in data["Time Series (Daily)"][key1]){
        let name = key2.slice(key2.indexOf(" ")+1);
        obj[name] = data["Time Series (Daily)"][key1][key2];
      }
      dataTable.push(obj);
    }
    console.log("dataTable", dataTable);
    return (
      <ReactTable data={dataTable} columns={[{
          Header: "Date",
          accessor: "date"
        },{
          Header: "Open",
          accessor: "open"
        }, {
          Header: "High",
          accessor: "high"
        }, {
          Header: "Low",
          accessor: "low"
        }, {
          Header: "Close",
          accessor: "close"
        }, {
          Header: "Adjusted Close",
          accessor: "adjusted close"
        }, {
          Header: "Volume",
          accessor: "volume"
        }, {
          Header: "Dividend Amount",
          accessor: "dividend amount"
        }, {
          Header: "Split Coefficient",
          accessor: "split coefficient"
        }]}
        defaultPageSize={10}
        className="-striped"/>
    )
  }
}

export default Table
