import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Redirect,Switch,Link} from "react-router-dom";
import Home from "../components/Home";
import Graph from "../components/Graph";
import Table from "../components/Table";
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';

class Routes extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      data: null,
      dropdownOpen: false
    };
  }
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  componentWillMount(){
    const url = "https://www.alphavantage.co/query?apikey=demo&function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT";
    fetch(url).then(result => result.json()).then(
      (result) => {
        this.setState({data: result})
      },
      (error) => {
        console.log("Error: " + error);
      }
    )
  }

  render () {
    const {data} = this.state;
    if(!data) return <div>Loading..</div>
    return (<Router>
      <div>
      <Nav tabs className="nav">
          <NavItem className="nav_logo">
            <NavLink><Link to="/" className="nav_logo-text">LOGO</Link></NavLink>
          </NavItem>
          <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle nav caret>Graph or Table?</DropdownToggle>
            <DropdownMenu>
              <DropdownItem header><Link to="/graph">Graph</Link></DropdownItem>
              <DropdownItem disabled><Link to="/table">Table</Link></DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>

        <Route path="/" component={Home}/>
        <Route exact path="/graph" render={(props) => <Graph {...props} data={data}/>} />
        <Route path="/table" render={(props) => <Table {...props} data={data}/>} />
        </div>
      </Router>);
  }
}

export default Routes;
